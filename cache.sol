// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.2 <0.9.0;

import "@openzeppelin/contracts/utils/Strings.sol";

contract GeoCache {
    address public creator;
    Coordinates coordinates;
    mapping(address => int) public hasVoted; //0 is no value, 1 is like, 2 is dislike
    uint public likes;
    uint public dislikes;
    string public title;
    string public hint1;
    string public hint2;
    string public hint3;

    uint256 public constant RESOLUTION = 100000000000000;

    struct Coordinates {
        uint256 lat;
        uint256 long;
    }

    constructor(
        address _creator,
        Coordinates memory _coordinates,
        string memory _title,
        string memory _hint1,
        string memory _hint2,
        string memory _hint3
    ) {
        creator = _creator;
        coordinates = _coordinates;
        title = _title;
        hint1 = _hint1;
        hint3 = _hint3;
        hint2 = _hint2;
        like(creator);
    }

    function coordinatesToString() public view returns (string memory) {
        string memory s1 = string.concat(
            '{"lat":"',
            Strings.toString(coordinates.lat)
        );
        string memory s2 = string.concat(s1, '","long":"');
        string memory s3 = string.concat(
            s2,
            Strings.toString(coordinates.long)
        );
        string memory s4 = string.concat(s3, '"}');
        return s4;
    }

    function like(address origin) public {
        require(hasVoted[origin] != 1, "You have already liked.");
        if (hasVoted[origin] == 2) {
            dislikes--;
        }
        hasVoted[origin] = 1;
        likes++;
    }

    function dislike(address origin) public {
        require(hasVoted[origin] != 2, "You have already disliked.");
        if (hasVoted[origin] == 1) {
            likes--;
        }
        hasVoted[origin] = 2;
        dislikes++;
    }

    function getHint(uint256 hintNumber) public view returns (string memory) {
        if (hintNumber == 1) {
            return hint1;
        } else if (hintNumber == 2) {
            return hint2;
        } else {
            return hint3;
        }
    }

    function seeVoted(address origin) public view returns (uint) {
        return hasVoted[origin];
    }

    function toString() public view returns (string memory) {
        string[9] memory string_parts = [
            '{"title":"',
            title,
            '","coords":',
            coordinatesToString(),
            ',"likes":"',
            Strings.toString(likes),
            '","dislikes":"',
            Strings.toString(dislikes),
            '"}'
        ];

        string memory string_build = string.concat(
            string_parts[0],
            string_parts[1]
        );
        for (uint i = 2; i < string_parts.length; i++) {
            string_build = string.concat(string_build, string_parts[i]);
        }

        return string_build;
    }
}
