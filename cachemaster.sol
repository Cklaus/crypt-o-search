// SPDX-License-Identifier: GPL-3.0

import "./cache.sol";

pragma solidity >=0.8.2 <0.9.0;

contract CacheMaster {
    GeoCache[] public caches;

    function createCache(
        uint256 lat,
        uint256 long,
        string memory _title,
        string memory _hint1,
        string memory _hint2,
        string memory _hint3
    ) public {
        caches.push(
            new GeoCache(
                msg.sender,
                GeoCache.Coordinates(lat, long),
                _title,
                _hint1,
                _hint2,
                _hint3
            )
        );
    }

    function heJoWTFHeBJiJVOorMiJ() public view returns (string memory) {
        string memory allCaches = "[";
        for (uint i = 0; i < caches.length; i++) {
            GeoCache cache = caches[i];
            allCaches = string.concat(allCaches, cache.toString());
            if (i < caches.length - 1) {
                allCaches = string.concat(allCaches, ",");
            }
        }
        string memory finalString = string.concat(allCaches, "]");
        return finalString;
    }

    function getHint(
        uint256 cacheIndex,
        uint256 hintNumber
    ) public view returns (string memory) {
        if (hintNumber > 3 || hintNumber <= 0) {
            revert(
                "a cache only has 3 hints, so hintNumber should be 1,2 or 3"
            );
        } else if (cacheIndex >= caches.length) {
            revert("index out of bounds of the cache array");
        } else {
            return caches[cacheIndex].getHint(hintNumber);
        }
    }

    function likeCache(uint256 cacheIndex) public {
        require (cacheIndex < caches.length, "invalid cache number");
        caches[cacheIndex].like(msg.sender);
    }

    function dislikeCache(uint256 cacheIndex) public {
        require (cacheIndex < caches.length, "invalid cache number");
        caches[cacheIndex].dislike(msg.sender);
    }

    function seeVoted(uint256 cacheIndex) public view returns (int) {
        require (cacheIndex < caches.length, "invalid cache number");
        return caches[cacheIndex].seeVoted(msg.sender);
    }
}
