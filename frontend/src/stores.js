import { writable } from 'svelte/store';

export const showModal = writable(false);

export const long = writable(false);

export const lat = writable(false);
